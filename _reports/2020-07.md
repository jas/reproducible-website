---
layout: report
year: "2020"
month: "07"
title: "Reproducible Builds in July 2020"
draft: false
date: 2020-08-08 15:52:47
---

[![]({{ "/images/reports/2020-07/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to the July 2020 report from the [Reproducible Builds](https://reproducible-builds.org) project.**
{: .lead}

In these monthly reports, we round-up the things that we have been up to over the past month. As a brief refresher, the motivation behind the Reproducible Builds effort is to ensure no flaws have been introduced from the original free software source code to the pre-compiled binaries we install on our systems. (If you're interested in contributing to the project, [please visit our main website]({{ "/" | relative_url }}).)

## General news

[![]({{ "/images/reports/2020-07/debconf20.png#right" | relative_url }})](https://debconf20.debconf.org/talks/49-reproducing-bullseye-in-practice/)

At the upcoming [DebConf20](https://debconf20.debconf.org/) conference (now [being held online](https://debconf20.debconf.org/news/2020-06-08-debconf20-moves-online/)), Holger Levsen will present a talk on Thursday 27th August about "[*Reproducing Bullseye in practice*](https://debconf20.debconf.org/talks/49-reproducing-bullseye-in-practice/)", focusing on independently verifying that the binaries distributed from `ftp.debian.org` were made from their claimed sources.

Tavis Ormandy published a blog post making the provocative claim that "[*You don't need reproducible builds*](http://blog.cmpxchg8b.com/2020/07/you-dont-need-reproducible-builds.html)", asserting elsewhere that the many attacks that have been extensively reported in our previous reports are ["fantasy threat models"](https://twitter.com/taviso/status/1288269090075754496). A number of rebuttals have been made, including [one from long-time contributor Reproducible Builds contributor Bernhard Wiedemann](https://rb.zq1.de/other/tavis.html).

[![]({{ "/images/reports/2020-07/openorienteering-mapper.png#right" | relative_url }})](https://www.openorienteering.org/apps/mapper/)

On our mailing list this month, Debian Developer Graham Inggs [posted to our list asking for ideas](https://lists.reproducible-builds.org/pipermail/rb-general/2020-July/001980.html) why the [`openorienteering-mapper`](https://tracker.debian.org/pkg/openorienteering-mapper) Debian package was failing to build on the [Reproducible Builds testing framework](https://tests.reproducible-builds.org). Chris Lamb remarked from the build logs that the package [may be missing a build dependency](https://lists.reproducible-builds.org/pipermail/rb-general/2020-July/001984.html), although Graham then used our own [*diffoscope*](https://diffoscope.org/) tool to show that the resulting package remains unchanged with or without it. Later, Nico Tyni noticed that the build failure may be due to the relationship between the [`FILE` C preprocessor macro and the `-ffile-prefix-map` GCC flag](https://lists.reproducible-builds.org/pipermail/rb-general/2020-July/001988.html).

An issue in [Zephyr](https://www.zephyrproject.org/), a small-footprint kernel designed for use on resource-constrained systems, around [`.a` library files not being reproducible](https://github.com/zephyrproject-rtos/zephyr/pull/17494) was closed after it was noticed that a key part of their toolchain was updated that [now calls `--enable-deterministic-archives` by default](https://github.com/zephyrproject-rtos/sdk-ng/issues/81).

[![]({{ "/images/reports/2020-07/libsodium.png#right" | relative_url }})](https://doc.libsodium.org/)

Reproducible Builds developer *kpcyrd* commented on a [pull request against the *libsodium* cryptographic library wrapper for Rust](https://github.com/sodiumoxide/sodiumoxide/pull/418#issuecomment-653692194), arguing against the testing of CPU features at compile-time. He noted that:

> I've accidentally shipped broken updates to users in the past because the build system was feature-tested and the final binary assumed the instructions would be present without further runtime checks

[David Kleuker](https://davidak.de) also asked a question on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general) about using [`SOURCE_DATE_EPOCH` with the `install(1)` tool](https://lists.reproducible-builds.org/pipermail/rb-general/2020-July/001991.html) from [GNU coreutils](https://www.gnu.org/software/coreutils/). When comparing two installed packages he noticed that the filesystem 'birth times' differed between them. [Chris Lamb replied](https://lists.reproducible-builds.org/pipermail/rb-general/2020-July/001995.html), realising that this was actually a consequence of using an outdated version of [*diffoscope*](https://diffoscope.org/) and that a fix was in [*diffoscope* version 146](https://diffoscope.org/news/diffoscope-146-released/) released in May 2020.

Later in July, John Scott posted asking for [clarification regarding on the Javascript files on our website](https://lists.reproducible-builds.org/pipermail/rb-general/2020-July/001999.html) to add metadata for [LibreJS](https://www.gnu.org/software/librejs/), the browser extension that blocks non-free Javascript scripts from executing. Chris Lamb investigated the issue and realised that we could drop a number of unused Javascript files [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3b71cb0)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/222b306)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/539eb99)] and added unminified versions of [Bootstrap](https://getbootstrap.com/) and [jQuery](https://jquery.com/) [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/fb57181)].

<br>

## Development work

### [Website](https://reproducible-builds.org/)

[![]({{ "/images/reports/2020-07/website.png#right" | relative_url }})](https://reproducible-builds.org/)

On our website this month, Chris Lamb updated the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) to drop a number of unused Javascript files [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3b71cb0)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/222b306)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/539eb99)] and added unminified versions of [Bootstrap](https://getbootstrap.com/) and [jQuery](https://jquery.com/) [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/fb57181)]. He also fixed a number of broken URLs [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/02be515)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/bf9e8ee)].

Gonzalo Bulnes Guilpain made a large number of grammatical improvements [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/81ee324)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3685ff3)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/9aa3796)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/9cb4ffa)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/dfdde38)] as well as some misspellings, case and whitespace changes too [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b06b9d1)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/1b86b33)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/04e943e)].

Lastly, Holger Levsen updated the `README` file&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c097558)], marked the [Alpine Linux](https://alpinelinux.org/) continuous integration tests as currently disabled&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/70b6ac5)] and linked the [Arch Linux Reproducible Status](https://reproducible.archlinux.org/) page from our [projects page]({{ "/who/" | relative_url }})&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3f143dd)].

### [diffoscope](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility that can not only locate and diagnose reproducibility issues, it provides human-readable diffs of all kinds. In July, Chris Lamb made the following changes to *diffoscope*, including releasing versions `150`, `151`, `152`, `153` & `154`:

* New features:

    * Add support for flash-optimised [F2FS](https://en.wikipedia.org/wiki/F2FS) filesystems.&nbsp;([#207](https://salsa.debian.org/reproducible-builds/diffoscope/issues/207))
    * Don't require `zipnote(1)` to determine differences in a `.zip` file as we can use `libarchive`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a8c9d69)]
    * Allow `--profile` as a synonym for `--profile=-`, ie. write profiling data to standard output.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ea8d111)]
    * Increase the minimum length of the output of `strings(1)` to eight characters to avoid unnecessary diff noise.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/572fc28)]
    * Drop some legacy argument styles: `--exclude-directory-metadata` and `--no-exclude-directory-metadata` have been replaced with `--exclude-directory-metadata={yes,no}`.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/aa4a109)]

* Bug fixes:

    * Pass the absolute path when extracting members from [SquashFS](https://en.wikipedia.org/wiki/SquashFS) images as we run the command with working directory in a temporary directory.&nbsp;([#189](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d226cf8))
    * Correct adding a comment when we cannot extract a filesystem due to missing [libguestfs](http://libguestfs.org/) module.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0dada5a)]
    * Don't crash when listing entries in archives if they don't have a listed size such as hardlinks in ISO images.&nbsp;([#188](https://salsa.debian.org/reproducible-builds/diffoscope/issues/188))

* Output improvements:

    * Strip off the file offset prefix from `xxd(1)` and show bytes in groups of 4.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/795f7ca)]
    * Don't emit `javap not found in path` if it is available in the path but it did not result in an actual difference.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d9ff65b)]
    * Fix `... not available in path` messages when looking for Java decompilers that used the Python class name instead of the command.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/eb79175)]

* Logging improvements:

    * Add a bit more debugging info when launching [libguestfs](http://libguestfs.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/69c0d4b)]
    * Reduce the `--debug` log noise by truncating the `has_some_content` messages.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a45f01c)]
    * Fix the `compare_files` log message when the file does not have a literal name.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9dee945)]

* Codebase improvements:

    * Rewrite and rename `exit_if_paths_do_not_exist` to not check files multiple times.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4d89836)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d43d580)]
    * Add an `add_comment` helper method; don't mess with our internal list directly.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d5ed790)]
    * Replace some simple usages of `str.format` with Python 'f-strings' [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5f41afe)] and make it easier to navigate to the `main.py` entry point [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/aee4f73)].
    * In the [RData](https://www.r-project.org/) comparator, always explicitly return `None` in the failure case as we return a non-`None` value in the success one.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/cd9e526)]
    * Tidy some imports [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/065a7e4)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e01f3df)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3641688)] and don't alias a variable when we do not use it.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e349860)]
    * Clarify the use of a separate `NullChanges` quasi-file to represent missing data in the Debian package comparator [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a76b0f1)] and clarify use of a 'null' diff in order to remember an exit code.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1dbc0d6)]

* Other changes:

    * Profile the launch of [libguestfs](http://libguestfs.org/) filesystems.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7e3bc6d)]
    * Clarify and correct our contributing info.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/4caa2f0)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c359bc8)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c66a7f6)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6437888)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/42afe44)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7b3a34f)]

Jean-Romain Garnier also made the following changes:

* Allow passing a file with a list of arguments via `diffoscope @args.txt`.&nbsp;([!62](https://salsa.debian.org/reproducible-builds/diffoscope/merge_requests/-/62))
* Improve the output of side-by-side diffs by detecting added lines better.&nbsp;([!64](https://salsa.debian.org/reproducible-builds/diffoscope/commit/65696a9))
* Remove offsets before instructions in `objdump`&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3e72c1c)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/cbcb41e)] and remove raw instructions from [ELF](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format) tests&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e9e2904)].

### Other tools

[![]({{ "/images/reports/2020-07/strip-nondeterminism.png#right" | relative_url }})](https://tracker.debian.org/pkg/strip-nondeterminism)

[*strip-nondeterminism*](https://tracker.debian.org/pkg/strip-nondeterminism) is our tool to remove specific non-deterministic results from a completed build. It is used automatically in most Debian package builds. In July, Chris Lamb ensured that we did not install the internal handler documentation generated from [Perl POD documents](https://perldoc.perl.org/perlpod.html) [[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/b9b8428)] and fixed a trivial typo [[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/3398261)]. Marc Herbert added a `--verbose`-level warning when the [Archive::Cpio](https://metacpan.org/pod/Archive::Cpio) Perl module is missing.&nbsp;([!6](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/merge_requests/-/6))

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is our end-user tool to build same source code twice in widely differing environments and then checks the binaries produced by each build for any differences. This month, Vagrant Cascadian made a number of changes to support [diffoscope version 153](https://diffoscope.org/news/diffoscope-153-released/) which had removed the (deprecated) `--exclude-directory-metadata` and `--no-exclude-directory-metadata` command-line arguments, and updated the testing configuration to also test under Python version 3.8&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/49e1701)].

<br>

## Distributions

#### [Debian](https://debian.org/)

[![]({{ "/images/reports/2020-07/debian.png#right" | relative_url }})](https://debian.org/)

In June 2020, Timo Röhling filed a [wishlist bug against the `debhelper` build tool](https://bugs.debian.org/962474) impacting the reproducibility status of hundreds of packages that use the [CMake build system](https://cmake.org/). This month however, Niels Thykier uploaded `debhelper` version 13.2 that passes the `-DCMAKE_SKIP_RPATH=ON` and `-DBUILD_RPATH_USE_ORIGIN=ON` arguments to CMake when using the (currently-experimental) Debhelper compatibility level 14.

According to Niels, this change:

> ... should fix some reproducibility issues, but may cause breakage if packages run binaries directly from the build directory.

34 reviews of Debian packages were added, 14 were updated and 20 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Chris Lamb added and categorised the `nondeterministic_order_of_debhelper_snippets_added_by_dh_fortran_mod` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/e67f706e)] and `gem2deb_install_mkmf_log` [[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/c5cd0e73)] toolchain issues.

Lastly, Holger Levsen filed two more wishlist bugs against the [`debrebuild`](https://salsa.debian.org/debian/devscripts/-/blob/master/scripts/debrebuild.pl) Debian package rebuilder tool [[...](https://bugs.debian.org/964722)][[...](https://bugs.debian.org/964733)].

#### [openSUSE](https://www.opensuse.org/)

[![]({{ "/images/reports/2020-07/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann published his [monthly Reproducible Builds status update](https://lists.opensuse.org/opensuse-factory/2020-07/msg00417.html).

Bernhard also published the [results of performing 12,235 verification builds](https://lists.opensuse.org/opensuse-factory/2020-07/msg00388.html) of packages from openSUSE Leap version 15.2 and, as a result, created three pull requests against the openSUSE [Build Result Compare Script](https://build.opensuse.org/package/show/openSUSE:Tools/build-compare) [[...](https://github.com/openSUSE/build-compare/pull/36)][[...](https://github.com/openSUSE/build-compare/pull/37)][[...](https://github.com/openSUSE/build-compare/pull/38)].

#### Other distributions

[![]({{ "/images/reports/2020-07/archlinux.png#right" | relative_url }})](https://www.archlinux.org/)

In [Arch Linux](https://www.archlinux.org/), there was a mass rebuild of old packages in an attempt to make them reproducible. This was performed because building with a previous release of the [pacman](https://www.archlinux.org/pacman/) package manager caused file ordering and size calculation issues when using the [btrfs](https://en.wikipedia.org/wiki/Btrfs) filesystem.

A system was also implemented for Arch Linux packagers to receive notifications if/when their package becomes unreproducible, and packagers now have access to a dashboard where they can all see all their unreproducible packages ([more info](https://lists.archlinux.org/pipermail/arch-dev-public/2020-July/030029.html)).

Paul Spooren sent two versions of a patch for the [OpenWrt](https://openwrt.org/) embedded distribution for adding a 'build system' revision to the 'packages' manifest so that all external feeds can be rebuilt and verified.&nbsp;[[...](http://lists.openwrt.org/pipermail/openwrt-devel/2020-July/030325.html)][[...](http://lists.openwrt.org/pipermail/openwrt-devel/2020-July/030171.html)]

## Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of these patches, including:

* Bernhard M. Wiedemann:

    * [`afl`](https://github.com/AFLplusplus/AFLplusplus/pull/441) (fix an incorrectly built manual page varied from kernel boot options)
    * [`brp-check-suse`](https://github.com/openSUSE/brp-check-suse/pull/32) (sorting issue)
    * [`dnscrypt-proxy`](https://build.opensuse.org/request/show/822151) (sort the output of `find(1)`)
    * [`graphviz`](https://gitlab.com/graphviz/graphviz/-/merge_requests/1454) (timezone issue, forwarded from Debian)
    * [`guile-gcrypt`](https://build.opensuse.org/request/show/818886) (parallelism)
    * [`insighttoolkit`](https://build.opensuse.org/request/show/823615) (prevent CPU detection, [forwarded upstream](https://github.com/InsightSoftwareConsortium/ITK/issues/1939)
    * [`ipopt`](https://build.opensuse.org/request/show/821088) (parallelism issue and use https://tracker.debian.org/pkg/strip-nondeterminism)
    * [`jboss-logging-tools`](https://build.opensuse.org/request/show/819312) (date, forwarded upstream)
    * [`kismet`](https://github.com/kismetwireless/kismet/pull/282) (date)
    * [`lcov`](https://build.opensuse.org/request/show/822800) (date issue, already upstream)
    * [`multus`](https://build.opensuse.org/request/show/819311) (date issue, already upstream)
    * [`multus`](https://github.com/intel/multus-cni/pull/534) (date)
    * [`paperjam`](https://mj.ucw.cz/sw/paperjam/) (date issue, forwarded upstream)
    * [`pspp`](https://build.opensuse.org/request/show/821090) (scrub `testsuite.log`)
    * [`python-PyNaCl`](https://github.com/pyca/pynacl/pull/609) (sort Python glob/readdir)
    * [`python-enaml`](https://build.opensuse.org/request/show/820537) (workaround an open upstream Python issue)
    * [`sac`](https://build.opensuse.org/request/show/822681) (omit creation time from `.zip` files)
    * [`sql-parser`](https://build.opensuse.org/request/show/818989) (sort, already upstream)
    * [`ugrep`](https://build.opensuse.org/request/show/818786) (CPU-related issue, already upstream)
    * [`ugrep`](https://github.com/Genivia/ugrep/pull/50) (CPU-related issue)
    * [`unknown-horizons`](https://build.opensuse.org/request/show/818930) (filesystem ordering issue, already upstream)
    * [`unknown-horizons`](https://github.com/unknown-horizons/unknown-horizons/pull/2943) (filesystem ordering issue)
    * [`xfce4-panel-profiles`](https://git.xfce.org/apps/xfce4-panel-profiles/about/) ([POSIX.1-2001/pax](https://en.wikipedia.org/wiki/Tar_(computing)#POSIX.1-2001/pax) headers)
    * [`yast2-sound`](https://github.com/yast/yast-sound/pull/50) (uses `uname -r`)

* Chris Lamb:

    * [`dh-fortran-mod`](https://tracker.debugs.debian.org/[#9652)
    * [`flit`](https://bugs.debian.org/964440)
    * [`gem2deb`](https://trabugs.debian.org/[#9647)
    * [`gmap`](https://bugs.debian.org/[#9649)
    * [`jskeus`](https://trbugs.debian.org/[#9661)
    * [`libtpms`](https://trabugs.debian.org/[#9647)
    * [`logilab-common`](https://tracker.debugs.debian.org/[#9652)
    * [`mrbayes`](https://trabugs.debian.org/[#9653)
    * [`nmap`](https://bugs.debian.org/964369)
    * [`numpydoc`](https://tracbugs.debian.org/[#9653)
    * [`pyerfa`](https://trbugs.debian.org/[#9649)
    * [`python-cooler`](https://tracker.dbugs.debian.org/[#9653)
    * [`python-peachpy`](https://bugs.debian.org/964186) ([forwarded upstream](https://github.com/Maratyszcza/PeachPy/pull/108))
    * [`python-pyxs`](https://trackerbugs.debian.org/[#9664)
    * [`sratom`](https://trbugs.debian.org/[#9665)
    * [`weather-util`](https://tracker.bugs.debian.org/964721)

* Guillaume Nodet:

    * [`apache-sshd`](https://github.com/apache/mina-sshd/commit/f5b261742e4f4d2ecc0052f948e88af58bda2b3d) (date)

Vagrant Cascadian also reported two issues, the first regarding a regression in [u-boot](https://www.denx.de/wiki/U-Boot) boot loader reproducibility for a particular target [[...](https://lists.denx.de/pipermail/u-boot/2020-July/420595.html)] and a non-deterministic segmentation fault in the [guile-ssh](https://github.com/artyom-poptsov/guile-ssh) test suite [[...](https://github.com/artyom-poptsov/guile-ssh/issues/22)]. Lastly, Jelle van der Waa filed a bug against the [MeiliSearch](https://www.meilisearch.com/) search API to report that it [embeds the current build date](https://github.com/meilisearch/MeiliSearch/issues/837).

## Testing framework

[![]({{ "/images/reports/2020-07/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

We operate a large and many-featured [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org).

This month, Holger Levsen made the following changes:

* [Debian](https://www.debian.org/)-related changes:

    * Tweak the rescheduling of various architecture and suite combinations.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/afd0f5cb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0af6eeb4)]
    * Fix links for '404' and 'not for us' icons.&nbsp;([#959363](https://bugs.debian.org/959363))
    * Further work on a rebuilder prototype, for example correctly processing the `sbuild` exit code.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cfa2ba45)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/19ce0560)]
    * Update the [sudo](https://www.sudo.ws/) configuration file to allow the node health job to work correctly.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/135c33a4)]
    * Add `php-horde` packages back to the `pkg-php-pear` package set for the *bullseye* distribution.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e9a7296f)]
    * Update the version of [`debrebuild`](https://salsa.debian.org/debian/devscripts/-/blob/master/scripts/debrebuild.pl).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2a2dd31d)]

* System health check development:

    * Add checks for broken SSH [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/967342cb)], `logrotate` [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/37ffd60f)], `pbuilder` [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/700d2775)], NetBSD [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/282b8ee3)], 'unkillable' processes&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/45d7925e)], unresponsive nodes&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/be118a1d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6e01c2d7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0f424acb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7c504518)], proxy connection failures&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/88987726)], too many installed kernels&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/199ea187)], etc.
    * Automatically fix some failed [`systemd`](https://www.freedesktop.org/wiki/Software/systemd/) units.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/052153a5)]
    * Add notes explaining all the issues that hosts are experiencing&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9e5ed290)] and handle zipped job log files correctly&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ced06afd)].
    * Separate nodes which have been automatically marked as down&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/283a5697)] and show status icons for jobs with issues&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/612bc046)].

* Misc:

    * Disable all [Alpine Linux](https://alpinelinux.org/) jobs until they are — or Alpine is — fixed.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ce107fb3)]
    * Perform some general upkeep of build nodes hosted by [OSUOSL](https://osuosl.org/).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/798efad2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5bb9c570)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d022f52c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2051e9d4)]

In addition, Mattia Rizzolo updated the `init_node` script to suggest using [sudo](https://www.sudo.ws/) instead of explicit logout and logins&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/99bdf68c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cf70ea0a)] and the usual build node maintenance was performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/25282617)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/00c41e98)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cbf6c3a8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/493a94c3)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b0823de3)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6cf7a07b)], Mattia Rizzolo [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/87cb7391)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/308d3cab)] and Vagrant Cascadian [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/89b77776)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4e100a5e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f2171a0d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0fd99b71)].


<br>
<hr>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mastodon: [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
