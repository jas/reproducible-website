---
layout: report
year: "2021"
month: "06"
title: "Reproducible Builds in June 2021"
draft: false
date: 2021-07-04 15:03:21
---

[![]({{ "/images/reports/2021-06/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

**Welcome to latest report from the [Reproducible Builds](https://reproducible-builds.org) project for June 2021.** In these reports we outline the most important things that have been happening in the world of reproducible builds in the past month. As ever, if you are interested in contributing to the project, please visit the [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

## Community news

[![]({{ "/images/reports/2021-06/lwn.png#right" | relative_url }})](https://lwn.net/Articles/859965/)

Jake Edge of [Linux Weekly News](https://lwn.net/) (LWN) published [a lengthy article on 16th June](https://lwn.net/Articles/859965/) describing various steps taken by the [Fedora Linux distribution](https://getfedora.org/) with respect to preventing supply-chain attacks:

> The specter of more events like the [SolarWinds supply-chain attacks](https://en.wikipedia.org/wiki/2020_United_States_federal_government_data_breach) is something that concerns many in our communities—and beyond. Linux distributions provide a supply chain that obviously needs to be protected against attackers injecting malicious code into the update stream. This problem recently came up on the Fedora devel mailing list, which led to a discussion covering a few different topics. For the most part, Fedora users are protected against such attacks, which is not to say there is nothing more to be done, of course.

<br>

[![]({{ "/images/reports/2021-06/google-open-source.png#right" | relative_url }})](https://security.googleblog.com/2021/06/introducing-slsa-end-to-end-framework.html)

The [Google Security Blog](https://security.googleblog.com/) introduced a new framework called "[*Supply chain Levels for Software Artifacts*](https://security.googleblog.com/2021/06/introducing-slsa-end-to-end-framework.html)", or SLSA (to be pronounced as 'salsa'). In particular, SLSA level 4 ("currently the highest level") not only requires a two-person review of all changes but also "a hermetic, reproducible build process" due to its "many auditability and reliability benefits". Whilst a highly welcome inclusion in Google's requirements, by equating reproducible builds with only the highest level of supply-chain security in their list, it might lead others to conclude that only the most secure systems can benefit from the benefits of reproducible builds, whilst it is a belief of the Reproducible Builds project that many more users, if not all, can do so.

<br>

Many media outlets (including [The Verge](https://www.theverge.com/2021/6/8/22524307/anom-encrypted-messaging-fbi-europol-afp-sting-operation-trojan-shield-greenlight), etc.) reported on how the United States' FBI operated a messaging app as a 'honeypot trap' for a long period of time, leading to hundreds of arrests. [According to the UK's Financial Times](https://www.ft.com/content/65ed6eb5-4968-4636-99bc-27a516d089dd), court documents describe how the FBI persuaded a software developer facing prison to allow the FBI to commandeer the app and to introduce it to suspected criminals:

[![]({{ "/images/reports/2021-06/anom.jpg#right" | relative_url }})](https://www.ft.com/content/65ed6eb5-4968-4636-99bc-27a516d089dd)

> Over the course of the next three years, the operation was able to inspect about 27m messages over 11,800 devices as ANOM gained popularity in criminal circles globally, pushed by the developer but also a network of crime "influencers" — experts in encrypted phones who encourage others to use such devices.

As the Financial Times reports, "it is unclear what exactly prompted the FBI and others to reveal the operation", although others have suggested it may result from legal limits in timeframes for intercepting communications. The FBI's operation raises ethical concerns which overlap with beliefs held by proponents of Reproducible Builds, not least of all because even the most unimpeachable actions by actors may result in the incidental surveillance of innocent people.

<br>

In similar legal news, Susan Landau posted to the [Lawfare blog](https://www.lawfareblog.com/) about the [potential dangers posted by evidentiary software](https://www.lawfareblog.com/dangers-posed-evidentiary-softwareand-what-do-about-it). In particular, she discusses concerns that proprietary software may be fundamentally incompatible with the ability of defendants have the right to know the nature of the evidence against them — this is a right that is explicitly enshrined, for instance, in the [Sixth Amendment](https://en.wikipedia.org/wiki/Sixth_Amendment_to_the_United_States_Constitution) of United States Constitution. However,

> At the time of our writing the article on the use of software as evidence, there was no overriding requirement that [United States] law enforcement provide a defendant with the code so that they might examine it themselves.

It is relevant here because if the inability to consult the relevant source code of does violate such rights, it may follow that a secure and reproducible build process will also be required — after all, it would be the output of the *binary* versions of the source code that is used to convict suspects, not the source code itself. As Susan points out:

> Mistakes happen with software and sometimes the only way to find errors is to study the code itself—both of which have important implications for courtroom use of software programs.

<br>

![]({{ "/images/reports/2021-06/reproducible-builds.png#right" | relative_url }})

The Reproducible Builds project restarted their IRC meetings this month. Taking place on the `#reproducible-builds` channel on the [OFTC IRC network](https://www.oftc.net/), the [log of the meeting on 29th June is now available](http://meetbot.debian.net/reproducible-builds/2021/reproducible-builds.2021-06-29-15.00.html) online, and the next meeting is due to take place on [**July 27th at 15:00 UTC**](https://time.is/compare/1500_27_Jul_2021_in_UTC) ([agenda](https://pad.riseup.net/p/rb-irc-meetings-keep)).

<br>

[Ars Technica](https://arstechnica.com/) are reporting that "counterfeit" packages in [PyPI](https://pypi.org/), the official Python package repository, [contained secret code that installed cryptomining software on infected machines](https://arstechnica.com/gadgets/2021/06/counterfeit-pypi-packages-with-5000-downloads-installed-cryptominers/): "So-called typosquatting attacks succeed when targets accidentally mistype a name such as typing *mplatlib* or *maratlib* instead of the legitimate and popular package, *matplotlib*". The article is at pains to points out that PyPI is not not abused any more than other repositories are:

> Last year, packages downloaded thousands of times from [RubyGems](https://arstechnica.com/information-technology/2020/04/725-bitcoin-stealing-apps-snuck-into-ruby-repository/) installed malware that attempted to intercept bitcoin payments. Two years before that, someone backdoored a 2-million-user code library hosted in NPM. [Sonatype](https://sonatype.com/) has [tracked more than 12,000 malicious NPM packages](https://blog.sonatype.com/open-source-attacks-on-the-rise-top-8-malicious-packages-found-in-npm) since 2019.

<br>

### Distribution work

[![]({{ "/images/reports/2021-06/alpine.png#right" | relative_url }})](https://ariadne.space/2021/06/04/a-slightly-delayed-monthly-status-update/)

[Ariadne Conill](https://ariadne.space/) published a [detailed blog post](https://ariadne.space/2021/06/04/a-slightly-delayed-monthly-status-update/) this month detailing their work on security issues and concerns in the [Alpine](https://alpinelinux.org/) Linux distribution. In particular, Ariadne included an interesting section on an effort "to prove the reproducibility of Alpine package builds":

> To this end, I hope to have the Alpine 3.15 build fully reproducible. This will require some changes to [`abuild`](https://wiki.alpinelinux.org/wiki/Abuild_and_Helpers) so that it produces `buildinfo files`, as well as a rebuilder backend. We plan to use the same buildinfo format as [Arch Linux](https://archlinux.org/), and will likely adapt some of the other reproducible builds work Arch has done to Alpine.

Ariadne mentioned plans to have a meeting and a sprint during July, to be organised in and around the `#alpine-reproducible` channel on the [OFTC IRC network](https://www.oftc.net/), and later posted a [round-up of security initiatives in Alpine during June](https://ariadne.space/2021/07/01/bits-relating-to-alpine-security-initiatives-in-june/) which mentions, amongst many other things, the ability to demonstrate [reproducible Alpine install images for the Raspberry Pi](https://twitter.com/sn0int/status/1410280462296268809).

Elsewhere in Alpine news, [*kpcyrd*](https://github.com/kpcyrd) posted a series of Tweets explaining the steps he made for a reproducible Alpine image.&nbsp;[[1](https://twitter.com/sn0int/status/1408853977106718724)] [[2](https://twitter.com/sn0int/status/1410280372051582978)]

For openSUSE, Bernhard M. Wiedemann posted his [monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/message/2EPZWDMEWIRO7FJE77JFGYTXDFNJPFCB/).

<br>

[![]({{ "/images/reports/2021-06/nixos.png#right" | relative_url }})](https://discourse.nixos.org/t/nixos-unstable-s-iso-minimal-x86-64-linux-is-100-reproducible/13723)

The [NixOS](https://nixos.org/) Linux distribution pulled off a technical and publicity coup this month by announcing that [*the `ISO_minimal.x86_64-Linux` image is 100% reproducible*](https://discourse.nixos.org/t/nixos-unstable-s-iso-minimal-x86-64-linux-is-100-reproducible/13723). The announcement was widely discussed on [Hacker News](https://news.ycombinator.com), where [the article has received in excess of 200 comments](https://news.ycombinator.com/item?id=27573393).

<br>

In early June, Nilesh Patra [asked for help making Debian's `brian` package build reproducibly](https://alioth-lists.debian.net/pipermail/reproducible-builds/Week-of-Mon-20210607/013052.html). Felix C. Stegerman proposed two patches which seem to have fixed the remaining issues ([#989693](https://bugs.debian.org/989693)). These were submitted upstream, where they were shortly merged.

<br>

[![]({{ "/images/reports/2021-06/fdroid.png#right" | relative_url }})](https://www.f-droid.org/)

Felix C. Stegerman announced the release of v1.0.0 of [*apksigcopier*](https://github.com/obfusk/apksigcopier), a tool to copy, extract and patch `.apk` signatures needed to facilitate reproducible builds on the [F-Droid](https://f-droid.org) Android application store. Holger Levsen subsequently sponsored an upload to Debian. Felix C. Stegerman also reported that Android builds are sometimes not reproducible due to a bug in Android's `coreLibraryDesugaring`.&nbsp;[[...](https://github.com/TeamNewPipe/NewPipe/issues/6486)]

Elsewhere in F-Droid, the Swiss COVID Certificate mobile app (which uses reproducible builds) has [been added to F-Droid](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/9099) — the F-Droid developers have mentioned that the upstream developers have been very helpful in making this happen. Relatedly, the Android version of the [Electrum Bitcoin Wallet](https://electrum.org/#home) [has been made reproducible](https://github.com/spesmilo/electrum/pull/7263).

<br>

[![]({{ "/images/reports/2021-06/mirageos.png#right" | relative_url }})](https://mirage.io)

Lastly, [Hannes Mehnert](https://hannes.robur.coop/) announced the launch of the [reproducible MirageOS build infrastructure](https://hannes.robur.coop/Posts/Deploy), together with where to [obtain 'unikernels'](https://builds.robur.coop): "To provide a high level of assurance and trust, if you distribute binaries in 2021, you should have a recipe how they can be reproduced in a bit-by-bit identical way."

<br>

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`deepdiff`](https://github.com/seperman/deepdiff/issues/255) (report a 'build failure in 2022' issue)
    * [`dulwich`](https://github.com/dulwich/dulwich/pull/885) (build fails in the future due to expired GPG key)
    * [`gtksourceview4`](https://bugzilla.opensuse.org/show_bug.cgi?id=1187842) (report that build fails in uniprocessor machine)
    * [`ipxe`](https://github.com/ipxe/ipxe/pull/388) (`ar(1)` call needs to be deterministic)
    * [`json-lib`](https://bugzilla.opensuse.org/show_bug.cgi?id=1187652) (report a date / epoch issue)
    * [`kernel-default`](https://bugzilla.suse.com/show_bug.cgi?id=1187167) (two sorting and random-related issues)
    * [`lepton`](https://build.opensuse.org/request/show/903137) (drop call to `-march=native`)
    * [`lighttpd1`](https://github.com/lighttpd/lighttpd1.4/pull/106) (build fails in 2036)
    * [`openvas-smb`](https://github.com/greenbone/openvas-smb/pull/40) (date and [Portable Executable](https://en.wikipedia.org/wiki/Portable_Executable) timestamp issue)
    * [`python-MapProxy`](https://github.com/mapproxy/mapproxy/issues/522) (report a 'build fails on uniprocessor machine' issue)
    * [`python-gcsfs`](https://bugzilla.opensuse.org/show_bug.cgi?id=1187516) (report a 'build fails on uniprocessor machine' issue)

* Nilesh Patra:

    * [#989572](https://bugs.debian.org/989572) filed against [`gl2ps`](https://tracker.debian.org/pkg/gl2ps).
    * [#989583](https://bugs.debian.org/989583) filed against [`liblip`](https://tracker.debian.org/pkg/liblip).
    * [#989693](https://bugs.debian.org/989693) filed against [`brian`](https://tracker.debian.org/pkg/brian).

* Vagrant Cascadian:

    * [#989963](https://bugs.debian.org/989963) filed against [`tclap`](https://tracker.debian.org/pkg/tclap).
    * [#989965](https://bugs.debian.org/989965) filed against [`gtk-sharp3`](https://tracker.debian.org/pkg/gtk-sharp3).
    * [#989966](https://bugs.debian.org/989966) filed against [`gtk-sharp3`](https://tracker.debian.org/pkg/gtk-sharp3).
    * [#990084](https://bugs.debian.org/990084) filed against [`graphicsmagick`](https://tracker.debian.org/pkg/graphicsmagick).
    * [#990246](https://bugs.debian.org/990246), [#990247](https://bugs.debian.org/990247) and [#990248](https://bugs.debian.org/990248) filed against [`vlc`](https://tracker.debian.org/pkg/vlc).
    * [#990253](https://bugs.debian.org/990253) filed against [`pmix`](https://tracker.debian.org/pkg/pmix).
    * [#990254](https://bugs.debian.org/990254) filed against [`openmpi`](https://tracker.debian.org/pkg/openmpi).
    * [#990300](https://bugs.debian.org/990300) filed against [`auctex`](https://tracker.debian.org/pkg/auctex).
    * [#990323](https://bugs.debian.org/990323) filed against [`volume-key`](https://tracker.debian.org/pkg/volume-key).
    * [#990327](https://bugs.debian.org/990327) filed against [`cppunit`](https://tracker.debian.org/pkg/cppunit).
    * [#990329](https://bugs.debian.org/990329) filed against [`rpm`](https://tracker.debian.org/pkg/rpm).
    * [#990332](https://bugs.debian.org/990332) filed against [`libcddb`](https://tracker.debian.org/pkg/libcddb).
    * [#990338](https://bugs.debian.org/990338) filed against [`autogen`](https://tracker.debian.org/pkg/autogen).
    * [#990339](https://bugs.debian.org/990339) filed against [`matplotlib`](https://tracker.debian.org/pkg/matplotlib).

Separate to this, Hans-Christoph Steiner noted there is a [reproducibility-related bug in Python's standard `zipfile` library](https://bugs.python.org/issue43547). This problem makes it hard to create reproducible `.zip` files. In particular, Hans would like to have more input from Python people, since it is not clear how best to resolve the problem.

<br>

### [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2021-06/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it provides human-readable diffs from many kinds of binary formats.

This month, [Chris Lamb](https://chris-lamb.co.uk) made a number of changes including releasing [version 177](https://diffoscope.org/news/diffoscope-177-released/)). In addition, Chris updated the [*try.diffoscope.org*](https://try.diffoscope.org) service to reflect that Bytemark were [acquired by the Iomart Group](https://blog.bytemark.co.uk/2018/09/04/moving-up-the-stack).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/try.diffoscope.org/commit/3e5c73a)].

* Balint Reczey:

    * Support `.deb` package members that are compressed with the [Zstandard](https://facebook.github.io/zstd/) compression algorithm.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7576e86d)]

* Jean-Romain Garnier:

    * Overhaul the [Mach-O](https://en.wikipedia.org/wiki/Mach-O) executable file comparator.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/39add067)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6a856a93)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/877fa55d)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c5f54f89)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/07123d53)]
    * Implement tests for the Mach-O comparator.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/521c85d8)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/459def43)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/cccebb94)]
    * Switch to new argument format for the [LLVM compiler](https://llvm.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a23c973f)]
    * Fix `test_libmix_differences` in testsuite for the [ELF format](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/88041849)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/fe81f577)]
    * Improve macOS compatibility for the Mach-O comparator.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f8900da8)]
    * Add `llvm-readobj` and `llvm-objdump` to the internal `EXTERNAL_TOOLS` data structure.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2853bd7d)]

* Mattia Rizzolo:

    * Invoke `gzip(1)` with its 'short' option names in order to support [Busybox](https://busybox.net/)'s version of the utility.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9aefdb65)]

<br>

### Website and documentation

[![]({{ "/images/reports/2021-06/website.png#right" | relative_url }})](https://reproducible-builds.org/)

A number of few changes were made to the [main Reproducible Builds website and documentation](https://reproducible-builds.org/) this month, including:

* Arnout Engelen:

    * Credit Ludovic Courtès for the [Guix](https://guix.gnu.org/) page.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/ed7e5c6)]
    * Fix link to [NixOS](https://nixos.org/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/380be47)]

* Chris Lamb:

    * Use an ellipsis [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/326bacd)] and drop a full stop [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/60347ac)] to clarify 'more items' links.
    * Update the link and logo to [Google Open Source Security Team](https://security.googleblog.com/).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/a1b9aed)]
    * Reduce the amount of bold text on [the homepage]({{ "/" | relative_url }}).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e9f44ea)]
    * Document the non-reproducibility arising from abbreviated Git hashes [depending on the number of total objects in a Git repository]({{ "/docs/version-information/" | relative_url }}#git-checksums).&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/31)]

* Hervé Boutemy:

    * Add a [Reproducible Central section]({{ "/docs/jvm/" | relative_url }}#reproducible-central) section to the JVM page.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/300587e)]

* Holger Levsen:

    * Add [*busybox*](https://busybox.net/) to the list of software respecting the [`SOURCE_DATE_EPOCH`]({{ "/specs/source-date-epoch/" | relative_url }}) environment variable for build timestamps if available.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e64f7d0)]

* Mattia Rizzolo:

    * Fix a typo in a CSS class name.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e2c8ee1)]
    * Add the (now-superseded) Linux Foundation [Core Infrastructure Initiative](https://www.coreinfrastructure.org/) to the list of historical sponsors.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/6d042f3)]

<br>

### Testing framework

[![]({{ "/images/reports/2021-06/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org). This month, the following changes were made:

* Holger Levsen:

    * Debian-related changes:

        * Initial stab at building and comparing [Debian Live](https://www.debian.org/CD/live/) images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cc531053)]
        * Run the `lb build` Debian Live command with `sudo(8)`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b777f20e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/250526ff)]
        * Use safer and more common `rm -rf` syntax in/around Debian Live images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/02402977)]
        * Sync build results of Live images to our Jenkins instance.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/811b5ab7)]
        * Create a Debian *unstable* schroot for running diffoscope on the `osuosl173` node so it can be used to test Debian Live images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b4080adf)]
        * Cope with the [Tails](https://tails.boum.org/) build manifests now only containing binary package names.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f7660c42)]
        * Do not incorrectly detect diskspace issues on [OpenSSL](https://www.openssl.org/) builds.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/51a40c40)]
        * Delete the `reproducible_compare_Debian_sha1sums` jobs.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e8fc4f71)]

    * Automatic node health check improvements:

        * Detect non-fatal failures using a [HTTP(S) proxy](https://en.wikipedia.org/wiki/Proxy_server#Web_proxy_servers).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4edbe7ca)]
        * Detect failure to "make tools".&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/75389717)]
        * Also detect "no route to host" issues.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/aef1c204)]
        * Tune regular expression to detect proxy failures.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bbaad8a1)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a7feb4a6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d70fca31)]
        * Misc aesthetic changes to the status page.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/99228d13)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a77f4de9)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/457c6f30)]

    * Misc:

        * Configure the `needrestart` tool to restart all services automatically.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0aa17b9d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c1fe64b8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a729e95e)]
        * Increase the [Linux kernel inotify](https://en.wikipedia.org/wiki/Inotify) watch limit further on all hosts.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0204b7cf)]
        * Be more verbose when cloning [Coreboot](https://www.coreboot.org/) Git repository.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/558c36b6)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ee9eeb4b)]
        * Properly delete old `schroot` overlays.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/99ca0c3d)]

* Mattia Rizzolo:

    * Update the documentation regarding manual scheduling Debian builds to drop old references to the deprecated [Alioth](https://wiki.debian.org/Alioth) system.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7a500fe1)]
    * Update a number of IP addresses for `armhf` architecture machines.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b242c74a)]

* Roland Clobus spent significant time on automatically building [Debian Live](https://www.debian.org/CD/live/) images twice and [comparing the output if they differ](https://wiki.debian.org/ReproducibleInstalls/LiveImages) ([Jenkins job page](https://jenkins.debian.net/job/reproducible_debian_live_build/)). This included:

    * Actually build the images twice and compare the output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c2987737)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a5d19afd)]
    * Improve cleanup routines.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1006e099)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/36c954df)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ffb6bdd9)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/456664d6)]
    * Store the ISO output.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dbc9471d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1966e740)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5ce264f5)]
    * Various `sudo(8)`-related configuration changes.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cad52f63)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/abb8bef8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/41cdf7e7)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/31b0c6c5)]

* Vagrant Cascadian:

    * Document the access to the `armhf` architecture host servers.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/12dd6947)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1bfe9112)]
    * Update the number of `armhf` architecture jobs and machines.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bbf5e229)]
    * Add build jobs and [SSH keys](https://www.openssh.com/) (etc.) for various new machines.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/38eba435)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b8b85dba)]

Finally, build node maintenance was performed by Holger Levsen [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/93ee8ecb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/994026c2)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/57abb28c)], Mattia Rizzolo [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/033d2a6c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e39f00c3)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/956888c8)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/24aac701)] and Vagrant Cascadian [[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5f452539)].

<br>

### Misc development news

Dan Shearer from the [LumoSQL](https://lumosql.org) database project [posted to the *rb-general* mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/2021-June/002282.html) about reproducibility and microcode updates, emphasis ours:

> Here at LumoSQL we do repeated runs testing [SQLite](https://www.sqlite.org/index.html) of various versions and configurations, storing the results in an SQLite database. Here is an example of the kind of variation that justifies what some have called our 'too-fussy' test suite, a [*microcode update that changes behaviour from one day to another*](https://travisdowns.github.io/blog/2021/06/17/rip-zero-opt.html).

Finally, in [last month's report]({{ "/reports/2021-05" | relative_url }}) we wrote about Paul Spooren [proposing a patch](http://lists.busybox.net/pipermail/busybox/2021-May/088842.html) for the [BusyBox](https://www.busybox.net/) suite of UNIX utilities so that it uses [`SOURCE_DATE_EPOCH`]({{ "/specs/source-date-epoch/" | relative_url }}) for build timestamps if available. This was [merged during June](http://lists.busybox.net/pipermail/busybox/2021-June/088880.html) by Denys Vlasenko.

<br>

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter ([@ReproBuilds](https://twitter.com/ReproBuilds)) and Mastodon ([@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)).

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
