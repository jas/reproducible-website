---
layout: report
year: "2022"
month: "10"
title: "Reproducible Builds in October 2022"
draft: false
date: 2022-11-11 15:56:14
---

[![]({{ "/images/reports/2022-10/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

*Welcome to the [Reproducible Builds](https://reproducible-builds.org) report for October 2022!* In these reports we attempt to outline the most important things that we have been up to over the past month.

As ever, if you are interested in contributing to the project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

---

[![]({{ "/images/reports/2022-10/summit_photo.jpg#right" | relative_url }})]({{ "/events/venice2022/" | relative_url }})

<br>

Our [in-person summit this year]({{ "/events/venice2022/" | relative_url }}) was held in the past few days in Venice, Italy. Activity and news from the summit will therefore be covered in next month's report!

---

[![]({{ "/images/reports/2022-10/paper.png#right" | relative_url }})](https://www.computer.org/csdl/proceedings-article/sp/2023/933600a167/1He7XSTyRKE)

A new article related to reproducible builds was recently published in the [2023 IEEE Symposium on Security and Privacy](https://www.computer.org/csdl/proceedings/sp/2023/1He7WWuJExG). Titled [*Taxonomy of Attacks on Open-Source Software Supply Chains*](https://www.computer.org/csdl/proceedings-article/sp/2023/933600a167/1He7XSTyRKE) and authored by Piergiorgio Ladisa, Henrik Plate, Matias Martinez and Olivier Barais, their paper:

> […] proposes a general taxonomy for attacks on opensource supply chains, independent of specific programming languages or ecosystems, and covering all supply chain stages from code contributions to package distribution.

Taking the form of an [attack tree](https://en.wikipedia.org/wiki/Attack_tree), the paper covers 107 unique vectors linked to 94 real world supply-chain incidents which is then mapped to 33 mitigating safeguards including, of course, reproducible builds:

> *Reproducible Builds* received a very high utility rating (5) from 10 participants (58.8%), but also a high-cost rating (4 or 5) from 12 (70.6%). One expert commented that a ”reproducible build like used by Solarwinds now, is a good measure against tampering with a single build system” and another claimed this ”is going to be the single, biggest barrier”.

---

It was noticed this month that Solarwinds [published a whitepaper back in December 2021](https://www.solarwinds.com/resources/whitepaper/setting-the-new-standard-in-secure-software-development-the-solarwinds-next-generation-build-system) in order to:

> […] illustrate a concerning new reality for the software industry and illuminates the increasingly sophisticated threats made by outside nation-states to the supply chains and infrastructure on which we all rely.

The 12-month anniversary of the 2020 "[Solarwinds attack](https://en.wikipedia.org/wiki/2020_United_States_federal_government_data_breach)" (which SolarWinds Worldwide LLC itself calls the "SUNBURST" attack) was, of course, the likely impetus for publication.

---

Whilst collaborating on [making the Cyrus IMAP server reproducible](https://github.com/cyrusimap/cyrus-imapd/issues/3893), [Ellie Timoney](https://github.com/elliefm) asked why the Reproducible Builds testing framework uses two remarkably distinctive build paths when attempting to flush out builds that vary on the absolute system path in which they were built. In the case of the [Cyrus IMAP server](https://www.cyrusimap.org/), these happened to be:

* `/build/1st/cyrus-imapd-3.6.0~beta3/`
* `/build/2/cyrus-imapd-3.6.0~beta3/2nd/`

Asked why they vary in three different ways, [Chris Lamb listed in detail the motivation behind to each difference](https://github.com/cyrusimap/cyrus-imapd/issues/3893#issuecomment-1279441131).

---

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month:

* Daniel Garcia from [WalletScrutiny.com](https://walletscrutiny.com/) started a thread asking for [input on buttons with the Reproducible Builds logo](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002703.html), requesting design suggestions or other feedback. [[...](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002703.html)]

* [Arch Linux](https://archlinux.org/) contributor *kpcyrd* wrote to our list this month with the news that "multiple people in Arch Linux noticed the output of our `git archive` command doesn't match the tarball served by [GitHub](https://github.com/) anymore. [In his post](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002709.html), *kpcyrd* narrows the change to a [specific commit in Git](https://github.com/git/git/commit/4f4be00d302bc52d0d9d5a3d4738bb525066c710). [[...](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002709.html)]

* Akihiro Suda wrote to a share a new tool called [`repro-get`](https://github.com/reproducible-containers/repro-get). According to [Akihiro's post](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002716.html), "repro-get is a tool to install a specific snapshot of apt/dnf/apk/pacman packages using SHA256SUMS files". This is needed in order to install specific (or "pinned") dependencies needed to validate a build.

* Finally, Janneke Nieuwenhuizen [announced the release of GNU Mes 0.24.1](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002708.html), which represents 23 commits over five months by four people. [GNU Mes](https://www.gnu.org/software/mes/) is a Scheme interpreter and C compiler for bootstrapping the GNU System. [[...](https://lists.reproducible-builds.org/pipermail/rb-general/2022-October/002708.html)]

---

[![]({{ "/images/reports/2022-10/openEuler.png#right" | relative_url }})](https://www.openeuler.org/)

The Reproducible Builds project is delighted to welcome [openEuler](https://www.openeuler.org/) to the [*Involved projects*]({{ "/who/projects/" | relative_url }}) page [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/bfc7b803)]. openEuler is Linux distribution developed by [Huawei](https://www.huawei.com/), a counterpart to it's more commercially-oriented [EulerOS](https://developer.huaweicloud.com/ict/en/site-euleros/euleros).

---

### Debian

[![]({{ "/images/reports/2022-10/debian.png#right" | relative_url }})](https://debian.org/)

[Colin Watson](https://www.chiark.greenend.org.uk/~cjwatson/) wrote about his experience towards making the databases generated by the `man-db` UNIX manual page indexing tool:

> One of the people working on [reproducible builds] [noticed](https://bugs.debian.org/1010957) that man-db’s database files were an obstacle to [reproducibility]: in particular, the exact contents of the database seemed to depend on the order in which files were scanned when building it. The reporter proposed solving this by processing files in sorted order, but I wasn't keen on that approach: firstly because it would mean we could no longer process files in an order that makes it more efficient to read them all from disk (still valuable on rotational disks), but mostly because the differences seemed to point to other bugs.

Colin goes on to describe his approach to solving the problem, including fixing various fits of internal caching, and he ends his post with "None of this is particularly glamorous work, but it paid off".

---

Vagrant Cascadian announced on [our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/) another online sprint to help "clear the huge backlog of reproducible builds patches submitted" by performing NMUs ([Non-Maintainer Uploads](https://wiki.debian.org/NonMaintainerUpload)). The first such sprint took place on September 22nd, but another was held on October 6th, and another small one on October 20th. This resulted in the following progress:

* Chris Lamb:

    * [`ascii2binary`](https://tracker.debian.org/pkg/ascii2binary) (Fixed [#1020812](https://bugs.debian.org/1020812), [#998758](https://bugs.debian.org/998758) & [#1007421](https://bugs.debian.org/1007421))
    * [`bibclean`](https://tracker.debian.org/pkg/bibclean) (Fixed [#829754](https://bugs.debian.org/829754) & [#929036](https://bugs.debian.org/929036))
    * [`dradio`](https://tracker.debian.org/pkg/dradio) (Fixed [#1020814](https://bugs.debian.org/1020814))
    * [`leave`](https://tracker.debian.org/pkg/leave) (Fixed [#777403](https://bugs.debian.org/777403), [#967002](https://bugs.debian.org/967002) & [#999259](https://bugs.debian.org/999259))
    * [`libimage-imlib2-perl`](https://tracker.debian.org/pkg/libimage-imlib2-perl) (Fixed [#1020665](https://bugs.debian.org/1020665))
    * [`mailto`](https://tracker.debian.org/pkg/mailto) (Fixed [#998978](https://bugs.debian.org/998978) & [#777413](https://bugs.debian.org/777413))
    * [`remote-tty`](https://tracker.debian.org/pkg/remote-tty) (Fixed [#829721](https://bugs.debian.org/829721) & [#977280](https://bugs.debian.org/977280))
    * [`xcolmix`](https://tracker.debian.org/pkg/xcolmix) (Fixed [#1020748](https://bugs.debian.org/1020748), [#999219](https://bugs.debian.org/999219) & [#988018](https://bugs.debian.org/988018))
    * [`z80asm`](https://tracker.debian.org/pkg/z80asm) (Fixed [#939775](https://bugs.debian.org/939775) & [#1020875](https://bugs.debian.org/1020875))

* Holger Levsen:

    * [`libtheora`](https://tracker.debian.org/pkg/libtheora) (Fixed [#990843](https://bugs.debian.org/990843) & [#990844](https://bugs.debian.org/990844))
    * [`sgml-base`](https://tracker.debian.org/pkg/sgml-base) (Fixed [#1006646](https://bugs.debian.org/1006646) & [#929706](https://bugs.debian.org/929706))

* Vagrant Cascadian:

    * [`ario`](https://tracker.debian.org/pkg/ario) (Investigated [#828876](https://bugs.debian.org/828876))
    * [`cloop`](https://tracker.debian.org/pkg/cloop) (Fixed [#787996](https://bugs.debian.org/787996))
    * [`elvis-tiny`](https://tracker.debian.org/pkg/elvis-tiny) (Fixed [#829755](https://bugs.debian.org/829755) & [#901345](https://bugs.debian.org/901345))
    * [`hannah`](https://tracker.debian.org/pkg/hannah) (Fixed [#845782](https://bugs.debian.org/845782) & [#901260](https://bugs.debian.org/901260))
    * [`mc`](https://tracker.debian.org/pkg/mc) (Investigated [#828683](https://bugs.debian.org/828683))
    * [`mod-dnssd`](https://tracker.debian.org/pkg/mod-dnssd) (Submitted alternate fix for [#828752](https://bugs.debian.org/828752))
    * [`snake4`](https://tracker.debian.org/pkg/snake4) (Fixed [#829715](https://bugs.debian.org/829715) & [#913734](https://bugs.debian.org/913734))
    * [`the`](https://tracker.debian.org/pkg/the) (Fixed [#842550](https://bugs.debian.org/842550))
    * [`zephyr`](https://tracker.debian.org/pkg/zephyr) (Investigated [#828867](https://bugs.debian.org/828867) & [#1021374](https://bugs.debian.org/1021374))
    * [`msp430mcu`](https://tracker.debian.org/pkg/msp430mcu) (Fixed [#860275](https://bugs.debian.org/860275))
    * [`checkpw`](https://tracker.debian.org/pkg/checkpw) (Fixed [#777299](https://bugs.debian.org/777299) & [#1020887](https://bugs.debian.org/1020887))
    * [`madlib`](https://tracker.debian.org/pkg/madlib) (Fixed [#778946](https://bugs.debian.org/778946))

---

41 reviews of Debian packages were added, 62 were updated and 12 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). A number of issue types were updated too. [[1](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/c3feb10e)][[...](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/518f83f0)]

---

Lastly, Luca Boccassi [submitted a patch](https://salsa.debian.org/debian/debhelper/-/merge_requests/93) to `debhelper`, a set of tools used in the packaging of the majority of Debian packages. The patch addressed an issue in the `dh_installsysusers` utility so that the `postinst` post-installation script that `debhelper` generates the same data regardless of the underlying filesystem ordering.

---

### Other distributions

[![]({{ "/images/reports/2022-10/fdroid.png#right" | relative_url }})](https://www.f-droid.org/)

[F-Droid](https://f-droid.org/) is a community-run app store that provides free software applications for Android phones. This month, F-Droid changed their documentation and guidance to now explicitly encourage RB for new apps [[...](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/12002)][[...](https://gitlab.com/fdroid/fdroiddata/-/issues/2816)], and FC Stegerman created an [extremely in-depth issue on GitLab](https://gitlab.com/fdroid/fdroidserver/-/issues/1056) concerning the [APK signing block](https://source.android.com/docs/security/features/apksigning/v2). You can read more about F-Droid's approach to reproducibility in [our July 2022 interview with Hans-Christoph Steiner of the F-Droid Project]({{ "/news/2022/06/24/supporter-spotlight-hans-christoph-steiner-f-droid-project/" | relative_url }}).

In openSUSE, Bernhard M. Wiedemann published his usual [openSUSE monthly report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/GFJP5HGSAPHJ4S63D3PYQJ237EDCMBXQ/).

---

### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`asymptote`](https://github.com/vectorgraphics/asymptote/pull/351) (date-related issue)
    * [`fastjet-contrib`](https://build.opensuse.org/request/show/1010423) (sort nondeterminstic filesystem ordering)
    * [`forge`](https://build.opensuse.org/request/show/1009599) ([Sphinx](https://www.sphinx-doc.org/) "doctree" issue)
    * [`gau2grid`](https://build.opensuse.org/request/show/1009603) (output varies with `march=native`)
    * [`gosec`](https://github.com/securego/gosec/pull/887) (date-related issue)
    * [`helmfile`](https://github.com/helmfile/helmfile/pull/486) (date-related issue)
    * [`libnvme`](https://build.opensuse.org/request/show/1032570) (date-related issue)
    * [`moab`](https://build.opensuse.org/request/show/1008583) (CPU)
    * [`tcl`](https://bugzilla.opensuse.org/show_bug.cgi?id=1203982) (fails to build in 2038)
    * [`vectorscan`](https://build.opensuse.org/request/show/1032506) (output varies with `march=native`)
    * [`xz2/lzma`](https://github.com/alexcrichton/xz2-rs/issues/100) (Rust-related filesystem ordering)

* Chris Lamb:

    * [#891263](https://bugs.debian.org/891263) filed against [`puppet`](https://tracker.debian.org/pkg/puppet) back in early 2018 was [finally merged into Puppet](https://github.com/puppetlabs/puppet/pull/8916#issuecomment-1265615317) and was released in [Puppet 7.20.0](https://puppet.com/docs/puppet/7/puppet_index.html).
    * [#1021198](https://bugs.debian.org/1021198) filed against [`puppet-agent`](https://tracker.debian.org/pkg/puppet-agent).
    * [#1022777](https://bugs.debian.org/1022777) filed against [`tpm2-pytss`](https://tracker.debian.org/pkg/tpm2-pytss) ([forwarded upstream](https://github.com/tpm2-software/tpm2-pytss/pull/376)).

* Vagrant Cascadian:

    * [#1021331](https://bugs.debian.org/1021331) filed against [`cclive`](https://tracker.debian.org/pkg/cclive).
    * [#1021373](https://bugs.debian.org/1021373) filed against [`librep`](https://tracker.debian.org/pkg/librep).
    * [#1021374](https://bugs.debian.org/1021374) filed against [`zephyr`](https://tracker.debian.org/pkg/zephyr).
    * [#1021452](https://bugs.debian.org/1021452) filed against [`libdv`](https://tracker.debian.org/pkg/libdv).
    * [#1021454](https://bugs.debian.org/1021454) filed against [`dbview`](https://tracker.debian.org/pkg/dbview).
    * [#1021456](https://bugs.debian.org/1021456) filed against [`bwbasic`](https://tracker.debian.org/pkg/bwbasic).
    * [#1021457](https://bugs.debian.org/1021457) filed against [`olpc-powerd`](https://tracker.debian.org/pkg/olpc-powerd).
    * [#1021458](https://bugs.debian.org/1021458) filed against [`o3dgc`](https://tracker.debian.org/pkg/o3dgc).
    * [#1021461](https://bugs.debian.org/1021461) filed against [`icon`](https://tracker.debian.org/pkg/icon).
    * [#1021463](https://bugs.debian.org/1021463) filed against [`rdist`](https://tracker.debian.org/pkg/rdist).
    * [#1021464](https://bugs.debian.org/1021464) filed against [`stfl`](https://tracker.debian.org/pkg/stfl).
    * [#1021466](https://bugs.debian.org/1021466) filed against [`pacman`](https://tracker.debian.org/pkg/pacman).
    * [#1021469](https://bugs.debian.org/1021469) filed against [`lam`](https://tracker.debian.org/pkg/lam).
    * [#1021470](https://bugs.debian.org/1021470) filed against [`xsok`](https://tracker.debian.org/pkg/xsok).
    * [#1021471](https://bugs.debian.org/1021471) filed against [`python-djvulibre`](https://tracker.debian.org/pkg/python-djvulibre).
    * [#1021472](https://bugs.debian.org/1021472) filed against [`xzoom`](https://tracker.debian.org/pkg/xzoom).
    * [#1021473](https://bugs.debian.org/1021473) filed against [`nitpic`](https://tracker.debian.org/pkg/nitpic).
    * [#1021498](https://bugs.debian.org/1021498) filed against [`tcm`](https://tracker.debian.org/pkg/tcm).
    * [#1021509](https://bugs.debian.org/1021509) filed against [`xxkb`](https://tracker.debian.org/pkg/xxkb).
    * [#1021512](https://bugs.debian.org/1021512) filed against [`yersinia`](https://tracker.debian.org/pkg/yersinia).
    * [#1021513](https://bugs.debian.org/1021513) filed against [`centrifuge`](https://tracker.debian.org/pkg/centrifuge).
    * [#1021514](https://bugs.debian.org/1021514) and [#1021516](https://bugs.debian.org/1021516) filed against [`ssocr`](https://tracker.debian.org/pkg/ssocr).
    * [#1021518](https://bugs.debian.org/1021518) filed against [`jakarta-jmeter`](https://tracker.debian.org/pkg/jakarta-jmeter).
    * [#1021520](https://bugs.debian.org/1021520) filed against [`guymager`](https://tracker.debian.org/pkg/guymager).
    * [#1021521](https://bugs.debian.org/1021521) and [#1021522](https://bugs.debian.org/1021522) filed against [`crack`](https://tracker.debian.org/pkg/crack).
    * [#1021751](https://bugs.debian.org/1021751) filed against [`dc3dd`](https://tracker.debian.org/pkg/dc3dd).
    * [#1021789](https://bugs.debian.org/1021789) filed against [`dlt-viewer`](https://tracker.debian.org/pkg/dlt-viewer).
    * [#1021792](https://bugs.debian.org/1021792) and [#1021793](https://bugs.debian.org/1021793) filed against [`vart`](https://tracker.debian.org/pkg/vart).
    * [#1021799](https://bugs.debian.org/1021799) and [#1021800](https://bugs.debian.org/1021800) filed against [`pgrouting`](https://tracker.debian.org/pkg/pgrouting).
    * [#1021860](https://bugs.debian.org/1021860) filed against [`libsx`](https://tracker.debian.org/pkg/libsx).
    * [#1021893](https://bugs.debian.org/1021893) filed against [`device-tree-compiler`](https://tracker.debian.org/pkg/device-tree-compiler).
    * [#1022130](https://bugs.debian.org/1022130) filed against [`tsdecrypt`](https://tracker.debian.org/pkg/tsdecrypt).

* John Neffenger:

    * [`openjdk`](https://github.com/openjdk/jdk/pull/10070) (Fixed [JDK-8292892](https://bugs.openjdk.org/browse/JDK-8292892))

### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2022-10/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb prepared and uploaded versions `224` and `225` to Debian:

* Add support for comparing the text content of HTML files using [`html2text`](https://alir3z4.github.io/html2text/). [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/d647eb75)]
* Add support for detecting ordering-only differences in XML files. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/dbf5350f)]
* Fix an issue with detecting ordering differences. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/727b3c9e)]
* Use the capitalised version of "Ordering" consistently everywhere in output. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/844e00c7)]
* Add support for displaying font metadata using [`ttx(1)`](https://fonttools.readthedocs.io/en/latest/ttx.html) from the [*fonttools*](https://fonttools.readthedocs.io/en/latest/index.html) suite. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/fe8326a8)]
* Testsuite improvements:

    * Temporarily allow the `stable-po` pipeline to fail in the CI. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ac2eb11f)]
    * Rename the `order1.diff` test fixture to `json_expected_ordering_diff`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/1bb51831)]
    * Tidy the JSON tests. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7f0407a1)]
    * Use `assert_diff` over `get_data` and an manual assert within the XML tests. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/a6a0f90b)]
    * Drop the `ALLOWED_TEST_FILES` test; it was mostly just annoying. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/7b7d28f0)]
    * Tidy the `tests/test_source.py` file. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/06fd0c79)]

Chris Lamb also added a link to diffoscope's [OpenBSD](https://www.openbsd.org/) packaging on the [*diffoscope.org*](https://diffoscope.org) homepage [[...](https://salsa.debian.org/reproducible-builds/diffoscope-website/commit/cb122aa)] and Mattia Rizzolo fix an test failure that was occurring under with [LLVM](https://llvm.org/) 15&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/154ad453)].

### Testing framework

[![]({{ "/images/reports/2022-10/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project operates a comprehensive testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org) in order to check packages and other artifacts for reproducibility. In October, the following changes were made by Holger Levsen:

* Run the `logparse` tool to analyse results on the [Debian Edu](https://wiki.debian.org/DebianEdu/) build logs.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/db3efa21)]
* Install `btop(1)` on all nodes running Debian.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5e33a32a)]
* Switch Arch Linux from using SHA1 to SHA256.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/470e8e8d)]
* When checking Debian `debstrap` jobs, correctly log the tool usage.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5efaac25)]
* Cleanup more task-related temporary directory names when testing Debian packages.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/53e3f678)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7ed09046)]
* Use the `cdebootstrap-static` binary for the 2nd runs of the `cdebootstrap` tests.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b6cf19d5)]
* Drop [a workaround](https://bugs.debian.org/1020630) when testing OpenWrt and coreboot as the issue in *diffoscope* has now been fixed.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b048e50b)]
* Turn on an `rm(1)` warning into an "info"-level message.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/8e2e8aa7)]
* Special case the `osuosl168` node for running Debian *bookworm* already.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5db24ae9)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/889afec3)]
* Use the new `non-free-firmware` suite on the `o168` node.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/0d75fe0e)]

In addition, Mattia Rizzolo made the following changes:

* Ensure that 2nd build has a merged `/usr`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/1bd43360)]
* Only reconfigure the `usrmerge` package on Debian *bookworm* and above.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/474a47c3)]
* Fix `bc(1)` syntax in the computation of the percentage of unreproducible packages in the dashboard.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b410b49e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/25691e9e)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b153a66d)]
* In the `index_suite_` pages, order the package status to be the same order of the menu.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fbcab7bf)]
* Pass the `--distribution` parameter to the `pbuilder` utility.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/dc5a5284)]

Finally, Roland Clobus continued his work on testing Live Debian images. In particular, he extended the maintenance script to warn when workspace directories cannot be deleted.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/299812ee)]

<br>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
